#!/bin/sh

if type "brew" > /dev/null 2>&1; then
  printf "\n########\n#Let's make sure everything is up to date.\n########\n"
  export PATH=/usr/local/bin:/usr/local/sbin:${HOME}/.composer/vendor/bin:${PATH}
  brew prune
  brew update
  brew doctor
  if [[ $? -eq 0 ]] ; then
    printf "########\n# Homebrew in order, continuing\n########"
    brew install ansible
  else
    printf "########\n# Homebrew needs some work, exiting\n########"
    exit
  fi
else
  printf "# Nope! Installing Homebrew now.\n########\n"
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  echo "export PATH=/usr/local/bin:/usr/local/sbin:${HOME}/.composer/vendor/bin:${PATH}" >> ~/.bash_profile
  echo "export PATH=/usr/local/bin:/usr/local/sbin:${HOME}/.composer/vendor/bin:${PATH}" >> ~/.zshrc
  source ~/.bash_profile
fi